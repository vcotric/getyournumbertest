//
//  main.m
//  pushchat
//
//  Created by Aleksandar Markovic on 9/24/13.
//  Copyright (c) 2013 Aleksandar Markovic. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
